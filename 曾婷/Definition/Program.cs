﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Definition
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
           var res = compute.Add(2.3,19.6);
            Console.WriteLine("2.3+19.6="+res);

           var sub = compute.Minus(18, 2);
            Console.WriteLine("18-2="+sub);

            var mul = compute.Multiply(555, 863112);
            Console.WriteLine("555*863112="+mul);

            var div = compute.Divide(300.25m,285.21m);
            Console.WriteLine("300.25m/285.21m="+div);
        }
    }
}
