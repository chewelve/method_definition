﻿using System;

namespace Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            //加法
            Type type = new Type();
           var sum= type.add(10.3, 5.20, 20,3);
            Console.WriteLine(sum);
            //减法
            var sum2 = type.Reduction(30.5, 20.6);
            Console.WriteLine(sum2);
            //乘法
            var sum3 = type.Take(2, 4, 5);
            Console.WriteLine(sum3);
            //除法
            var sum4 = type.Division(50, 2,5);
            Console.WriteLine(sum4);

            //多数加减乘除
            var sum5 = type.add(sum, sum2, sum3, sum4);
            Console.WriteLine(sum5);
        }
    }
}
