﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp001
{
    public abstract class Animal
    {
        public void Dance()
        {
            Console.WriteLine("奔跑不如跳舞，跳舞");
        }
        public void Age()
        {
            Console.WriteLine("你肯定没我快长大");
        }
    }
    public class Dog : Animal
    {
        public void Dark()
        {
            Console.WriteLine("有一说一，你怕不知道什么是对牛弹琴");
        }
    }
}
