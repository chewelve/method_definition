﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefineMethods
{
    class Recursion
    {
        public int factorial(int num)
        {
             
            int result;

            if (num == 1)
            {
                return 1;
            }
            else
            {
                result = factorial(num - 1) * num;
                return result;
            }
        }
    }
}
