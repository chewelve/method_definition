﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var res = compute.Plus(10 , 20);
            Console.WriteLine(res);
            var res2 = compute.Reduce(50 , 10);
            Console.WriteLine(res2);
            var res3 = compute.Multiply(5 , 9);
            Console.WriteLine(res3);
            var res4 = compute.Divide(45, 5);
            Console.WriteLine(res4);
        }
    }
}
