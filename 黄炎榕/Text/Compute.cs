﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class Compute
    {
        //加法
        public double Add(double num1,double num2)
        {
            return num1 + num2;
        }

        //减法
        public double Sub(double num1,double num2)
        {
            return num1 - num2;
        }

        //乘法
        public double Mul(double num1, double num2)
        {
            return num1 * num2;
        }

        //除法
        public double Div(double num1, double num2)
        {
            return num1 / num2;
        }

        //取余数
        public int Rem(int num1, int num2)
        {
            return num1 % num2;
        }

        //阶乘
        public int Fac(int num)
        {
            if (num==1)
            {
                return 1;
            }
            else
            {
                int result = Fac(num - 1) * num;
                return result;
            }
        }
    }
}
