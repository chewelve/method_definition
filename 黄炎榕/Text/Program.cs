﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var add = compute.Add(6.6,8.8);
            var sub = compute.Sub(3.0,2.1);
            var mul = compute.Mul(1.5,2);
            var div = compute.Div(4,2);
            var rem = compute.Rem(2, 3);
            var fac = compute.Fac(6);
            Console.WriteLine("加法的值为：{0}，减法的值为：{1}，乘法的值为：{2}，除法的值为：{3}，余数的值为：{4}，六的阶乘为：{5}", add, sub, mul, div, rem, fac);
            Console.ReadKey();
        }
    }
}
